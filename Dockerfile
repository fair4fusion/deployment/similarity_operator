FROM python:3.9-slim

ENV FLASK_WORKERS=1
ENV FLASK_THREADS=4

RUN apt update && apt install -y git \
	gcc \
	make

RUN apt-get install python3-pip -y

COPY requirements.txt /tmp/requirements.txt
RUN pip3 install -r /tmp/requirements.txt

#RUN mkdir data/
WORKDIR /app
COPY app.py run_flask.sh /app/
RUN chmod 755 /app/run_flask.sh
EXPOSE 8000

#COPY similarity.py similarity.py
# COPY /out /out
# EXPOSE 8000
#ENTRYPOINT ["python3","similarity.py", "/out"]
CMD /app/run_flask.sh
