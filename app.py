import logging
import os
import sys
from flask import Flask, request
from flask_restful import Api
from heapq import nsmallest, nlargest
import numpy as np

app = Flask(__name__)

app.config.update({
    'TESTING': True
})

api = Api(app)


@app.route('/run', methods=['POST'])
def run():
    id = request.form['id']
    experimentId = request.form['experimentId']
    number_of_sim = int(request.form['number_of_sim'])
    fn = "/work/" + experimentId + "/inter_out/" + id
    if not os.path.exists(fn):
        os.makedirs(fn)
    fout = "/work/" + experimentId + "/out/" + id
    if not os.path.exists(fout):
        os.makedirs(fout)
    distances = []
    idd = []

    for filename in os.listdir(fn):
        if filename.endswith(".distance"):
            logging.error(filename)
            (file, ext) = os.path.splitext(filename)
            idd.append(int(file))
            with open(os.path.join(fn, filename), 'r') as f:
                value = np.fromfile(f, dtype=float, count=-1, sep=' ')
                distances.append(value)

    smallest_distances = nsmallest(number_of_sim, distances)
    largest_distances = nlargest(number_of_sim, distances)
    indices_smallest = []
    indices_largest = []
    for i in smallest_distances:
        if i in distances and i != 0:
            indices_smallest.append(distances.index(i))
    for i in largest_distances:
        if i in distances:
            indices_largest.append(distances.index(i))

    similar_values_smallest = []
    similar_values_largest = []
    for i in indices_smallest:
        similar_values_smallest.append(idd[i])
    for i in indices_largest:
        similar_values_largest.append(idd[i])
    with open(fout+"/output_smallest.txt", 'w') as f:
        for item in similar_values_smallest:
            f.write("%s\n" % item)
    with open(fout+"/output_largest.txt", 'w') as f:
        for item in similar_values_largest:
            f.write("%s\n" % item)

    with open(f'{fout}/status.file', 'w') as file:
        file.write("Succeeded")
        file.close

    return "Your request is over"


logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

if __name__ == '__main__':
    app.run(debug=True)

